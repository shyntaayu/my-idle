import { Disabilitas } from './disabilitas';

export interface Disabilitasresponse {
    code: number;
    data: Disabilitas[];
}