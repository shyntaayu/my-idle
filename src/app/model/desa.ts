export interface Desa {
    created_at: Date;
    id: number;
    desa: string;
    id_kecamatan: number;
}