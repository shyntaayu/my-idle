export interface Kecamatan {
    created_at: Date;
    id: number;
    kecamatan: string;
    id_kota: number;
}